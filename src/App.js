import React from 'react';
import './App.css';
import {Route, BrowserRouter as Router } from 'react-router-dom'
import mynukad from './component/mynukad'
import home from './component/home'
import view from './component/view'

function App() {
  return (
    <Router>
      <Route path="/" exact component={mynukad}/>
      {/* <Route path="/" exact component={home}/> */}
      <Route path="/viewdata" exact component={view}/>
    </Router>
  );
}

export default App;
